# conan.cmake

Instructs CMake prepare a `conanbuildinfo.cmake` file inside the current `CMAKE_BINARY_DIR`
based on the `conanfile.txt` expected in the `CMAKE_SOURCE_DIR`.


## Synopsis

In your `CMakeLists.txt` state
```
include(conan)
```

## Options

-


## Example

_TBD_


---


Copyright (C) 2020-2021 headcode.space e.U.  
Oliver Maurhart <info@headcode.space>  
[https://headcode.space](https://www.headcode.space)  
