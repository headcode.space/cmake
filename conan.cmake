# ------------------------------------------------------------
# This integrates conan cmake info build file into cmake
#
# The 'LICENSE.txt' file in the project root holds the software license.
# Copyright (C) 2020-2021 headcode.space e.U.
# Oliver Maurhart <info@headcode.space>, https://www.headcode.space
# ------------------------------------------------------------

find_program(CONAN conan)
if (CONAN-NOTFOUND)
    message(FATAL_ERROR "Conan package manager not found. Please install it (see: https://conan.io)")
endif ()

add_custom_command(OUTPUT conanbuildinfo.cmake
    COMMAND ${CONAN} install ${CMAKE_SOURCE_DIR}
    MAIN_DEPENDENCY ${CMAKE_SOURCE_DIR}/conanfile.txt
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Preparing conanbuildinfo.cmake"
)
add_custom_target(canonify ALL DEPENDS conanbuildinfo.cmake)

# first contact: enforce file creation in the first place
find_file(CONAN_BUILD_INFO_CMAKE conanbuildinfo.cmake ${CMAKE_BINARY_DIR})
if ("${CONAN_BUILD_INFO_CMAKE}" STREQUAL "CONAN_BUILD_INFO_CMAKE-NOTFOUND")
     execute_process(
         COMMAND ${CONAN} install ${CMAKE_SOURCE_DIR}
         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
     )
endif ()

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()
set(CMAKE_REQUIRED_INCLUDES ${CONAN_INCLUDE_DIRS})
